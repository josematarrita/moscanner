package com.example.moscanner.Data;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static android.support.constraint.Constraints.TAG;

@Singleton
public class DeviceRepository {

    /**
     * This class helps to handle the results after scanning for devices
     */
    private class BtleScanCallback extends ScanCallback {
        HashMap<String, BluetoothDevice> mScanResults;//results will be stored here

        public BtleScanCallback(HashMap<String, BluetoothDevice> mScanResults){
            this.mScanResults = mScanResults;
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            addScanResult(result);
        }
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                addScanResult(result);
            }
        }
        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "BLE Scan Failed with code " + errorCode);
        }
        private void addScanResult(ScanResult result) {
            BluetoothDevice device = result.getDevice();
            String deviceAddress = device.getAddress();
            mScanResults.put(deviceAddress, device);
        }
    }

    //Local variables
    private DeviceDao deviceDao; //avariable to access DAO
    private BluetoothAdapter bluetoothAdapter; ////variable to access the bluetooth adapter and its functions
    private final static int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private boolean mScanning; //boolean to know if its scanning
    private Handler handler; //the handler helps to handle the bluetooth
    BluetoothLeScanner mBluetoothLeScanner;
    BtleScanCallback mScanCallback;
    HashMap<String, BluetoothDevice> mScanResults;



    @Inject
    public DeviceRepository(BluetoothAdapter bluetoothAdapter, DeviceDao deviceDao){
        this.bluetoothAdapter = bluetoothAdapter;
        this.deviceDao = deviceDao;
    }

    // Addds an object device to the list of objects
    public void addDevice(Device device){
        deviceDao.addDevice(device);
    }


    //ensures the bluetooth is enabled.
    public void ensureEnabled(Activity activity){
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {

            Intent enableBtIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

        }
    }


    //scans device and put them in an array list, it stops after the scan period is passed
    public void scanLeDevice() {
        List<ScanFilter> filters = new ArrayList<>();
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();
        mScanResults = new HashMap<>();
        mScanCallback = new BtleScanCallback(mScanResults);
        mBluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        mBluetoothLeScanner.startScan(filters, settings, mScanCallback);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScanning = false;
                bluetoothAdapter.cancelDiscovery();
            }
        }, SCAN_PERIOD);

    }

    //stops scan
    public void stopScan() {
        if (mScanning && bluetoothAdapter != null && bluetoothAdapter.isEnabled() && mBluetoothLeScanner != null) {
            mBluetoothLeScanner.stopScan(mScanCallback);
            scanComplete();
        }

        mScanCallback = null;
        mScanning = false;
        handler = null;
    }

    //throws a message when a scan is complete and if devices were found.
    private void scanComplete() {
        if (mScanResults.isEmpty()) {
            return;
        }
        for (String deviceAddress : mScanResults.keySet()) {
            Log.d(TAG, "Found device: " + deviceAddress);
        }
    }


}
