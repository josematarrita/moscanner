package com.example.moscanner.Data;


import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


//I added this dao, which is fake as there's no real DB here, just to represent what could be use
// in a real app.
public class DeviceDao {

    //this is a list of the devices that are already scanned.
    private final List<Device> devicesConnected = new ArrayList();
    private final MutableLiveData<List<Device>> devices = new MutableLiveData<>();

    //initializes devices.
    void init(){
        devices.setValue(devicesConnected);
    }

    //add the device to the list
    public void addDevice(Device device){
        devicesConnected.add(device);
        devices.setValue(devicesConnected);
    }

    //returns the list with all the devices added
    public MutableLiveData getDevices(){
        return devices;
    }

    //deletes a device from the list
    public void deleteDevice(Device delDevice){
        devicesConnected.remove(delDevice);
        devices.setValue(devicesConnected);
    }


}
