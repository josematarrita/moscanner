package com.example.moscanner.Data;

import java.util.UUID;

public class Device {
    /*This would be the attributes of a table in a DB*/
    private String localName;
    private String data;
    private UUID identifier;

    //initialize the object
    public Device(String localName, String data, UUID identifier){
        this.localName = localName;
        this.data = data;
        this.identifier = identifier;
    }

    public String getLocalName(){
        return localName;
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setIdentifier(UUID identifier) {
        this.identifier = identifier;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }
}
