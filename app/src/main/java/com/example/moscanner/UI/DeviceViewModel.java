package com.example.moscanner.UI;

import android.arch.lifecycle.ViewModel;

import com.example.moscanner.Data.DeviceRepository;

public class DeviceViewModel extends ViewModel {

    DeviceRepository deviceRep; //variable for the repository

    //class constructor.
    public  DeviceViewModel(DeviceRepository deviceRep){
        deviceRep = deviceRep;
    }

    //method that starts scan
    public void onClickScan(){
        deviceRep.scanLeDevice();
    }

    //method that stops scan
    public void onClickStop(){
        deviceRep.stopScan();
    }
}
