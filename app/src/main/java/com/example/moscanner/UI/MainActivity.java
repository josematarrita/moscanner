package com.example.moscanner.UI;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.moscanner.R;
import com.example.moscanner.Utilities.InjectorModule;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity {

    //Local Variables
    InjectorModule bluetoothModule;
    private DeviceViewModel devVM;
    Context context;

    /*
    * The method on creates creates the activity, gets the context and calls the method setupUI.
    *
    * */
    @Override
    @Inject
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context =getApplicationContext();
        //setupUI(context);
    }


    /*SetupUI receives a Context variable named context which is used to try to  initilize de variable devM which is for the
    * ViewModel.*/
    protected void setupUI(Context context){
        devVM = bluetoothModule.createsViewModel(context);
    }


    /**
     * In the UI when you click the start button this method is called which is meant to start scanning for devices.
     */
    public void onStart(View v){
        devVM.onClickScan();
    }


    /**
     * In the UI when you click the stop button this method is called which is meant to stop scanning for devices.
     */
    public void onStop(View v){
        devVM.onClickStop();
    }

}
