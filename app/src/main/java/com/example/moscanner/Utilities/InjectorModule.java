package com.example.moscanner.Utilities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;

import com.example.moscanner.Data.DeviceDao;
import com.example.moscanner.Data.DeviceRepository;
import com.example.moscanner.UI.DeviceViewModel;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class InjectorModule {

    BluetoothAdapter bluetoothAdapter;
    DeviceRepository deviceRepository;
    DeviceViewModel devViewModel;
    DeviceDao deviceDao;

    /*
    * This initialize and provides a bluetooth adapter.
    *
    * */
    @Provides @Singleton
    public BluetoothAdapter getBluetoothAdapter(Context context) {
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        return bluetoothAdapter = bluetoothManager.getAdapter();

    }

    /**
     * Initializes the DAO, Adapter, Repository and viewModel for other clases to use them.
     *
     */
    @Provides
    public DeviceViewModel createsViewModel(Context context){
        getBluetoothAdapter(context);
        deviceDao = new DeviceDao();
        deviceRepository = new DeviceRepository(bluetoothAdapter, deviceDao);
        devViewModel = new DeviceViewModel(deviceRepository);
        return  devViewModel;
    }
}
