Application:

My appplication is a simple window with two buttons, one for scan and one to stop scanning. As of this version it only displays the activity, 
this is because a bug where i can't get the context, it always return null.

Problems faced:

The biggest problem was that I spent too much time researching the new concepts and ideas that I was supposed to use and left little time
to debug and fix issues. There was also a problem were I found a library that used JavaRx and BLE methods but i could not get it to work properly
and have to pivot of of that. 

Resources used:
I used the MVVM, Dagger2 and Android studio to develop this, however i didn't use the JavaRx because of what I wrote earlier.

I also used an emulator to test what i could.




